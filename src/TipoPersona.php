<?php

namespace idartes\usuario;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoPersona extends Model
{
    use SoftDeletes;
    protected $table = 'tbl_tipo_persona';
    protected $primaryKey = 'i_pk_id';
    protected $fillable = ['i_estado', 'user_id', 'tipo_id', 'd_fecha_inicio', 'd_fecha_fin', 'dt_fecha_posesion', 'dt_fecha_retiro', 'i_fk_id_area', 'vc_anexo_soporte_oficio', 'i_encargo', 'vc_anexo_ruta', 'vc_anexo_ruta_sistema'];

    public function __construct()
    {
        $this->connection = config('usuarios.conexionadm');
    }

    public function usuario()
    {
        return $this->belongsTo(config('usuarios.modelo_user'), 'user_id', 'id');
    }

    public function tipo()
    {
        return $this->belongsTo(config('usuarios.modelo_tipo'), 'tipo_id', 'i_pk_id');
    }

    public function parametroDetalles()
    {
        return $this->belongsTo('idartes\usuario\ParametroDetalles', 'i_fk_id_area', 'i_pk_id');
    }
}
