<?php

namespace idartes\usuario;

use Illuminate\Database\Eloquent\Model;

class DiaNoHabil extends Model
{

    protected $table = 'tbl_dias_no_habiles';
    protected $primaryKey = 'i_pk_id';

    protected $fillable = [
        'd_dia', 
        'vc_descripcion'
    ];
    public $timestamps = true;
}
