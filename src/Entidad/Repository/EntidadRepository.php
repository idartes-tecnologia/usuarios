<?php

namespace idartes\usuario\Entidad\Repository; 

use idartes\usuario\Entidad\Entidad;

class EntidadRepository implements EntidadInterface
{
	public function obtenerTodo()
    {
		return Entidad::all();
	}

    public function obtenerEntidadesCabezaSector()
    {
		return Entidad::where('i_cabeza_sector', 1)->get();
	}

    public function obtenerEntidadPrincipal()
    {
		return Entidad::where('i_principal', 1)->first();
	}

}