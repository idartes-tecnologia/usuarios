<?php

namespace idartes\usuario;

use Illuminate\Database\Eloquent\Model;


class Tipo extends Model
{
    
    protected $table = 'tbl_tipo';
    protected $primaryKey= 'i_pk_id';
    protected $fillable = ['vc_tipo', 'i_estado', 'i_fk_id_modulo','i_fk_id_clase','i_requisitos'];
  
    public function __construct()
    {
        $this->connection = config('usuarios.conexionadm');
    }

    public function tipoPersonas()
    {
        return $this->hasMany(config('usuarios.modelo_tipo_persona'), 'tipo_id', 'i_pk_id');
    }     
    
    public function Users()
    {
        return $this->belongsToMany(config('usuarios.modelo_user'), 'tbl_tipo_persona', 'tipo_id', 'user_id');
    }

    public function actividades()
    {
        return $this->belongsToMany(config('usuarios.modelo_actividad'), 'tbl_tipo_actividad', 'tipo_id', 'actividad_id');
    }

    public function modulos()
    {
        return $this->belongsTo(config('usuarios.modelo_modulo'), 'i_fk_id_modulo', 'i_pk_id');
    }


}