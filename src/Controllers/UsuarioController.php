<?php

namespace idartes\usuario\Controllers; 

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use idartes\usuario\Repository\ParametroDetalleRepository;
use idartes\usuario\Repository\UsuarioInterface;
use idartes\usuario\Requests\CrearUsuarioRequest;
use idartes\usuario\Requests\ActualizarUsuarioRequest; 
use idartes\usuario\Estado;
use idartes\usuario\User;

class UsuarioController extends Controller
{



    protected $usuarioRepository;
    protected $parametrosRepository;

    public function __construct(ParametroDetalleRepository $configuracion, UsuarioInterface $usuarioRepository ){
        $this->parametrosRepository=$configuracion;
        $this->usuarioRepository=$usuarioRepository;
    }  

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('/gestionar-usuarios'); 
    } 

    public function inicio(){
        $data = [
            'areas'=>$this->parametrosRepository->obtenerParametro(Estado::CODIGO_AREAS)->pluck('vc_parametro_detalle','i_pk_id')->toArray(),
            'areasPys'=>$this->parametrosRepository->obtenerParametro(Estado::CODIGO_AREAS_PAZ_Y_SALVOS)->pluck('vc_parametro_detalle','i_pk_id')->toArray(),
            'tipoDocumentos'=>$this->parametrosRepository->obtenerParametro(Estado::CODIGO_TIPO_DOCUMENTO)->pluck('vc_parametro_detalle','i_pk_id')->toArray(),
            'ciudades'=>$this->parametrosRepository->obtenerParametro(Estado::CODIGO_CIUDAD)->pluck('vc_parametro_detalle','i_pk_id')->toArray(),
            'generos'=>$this->parametrosRepository->obtenerParametro(Estado::CODIGO_GENERO)->pluck('vc_parametro_detalle','i_pk_id')->toArray(),
            'etnias'=>$this->parametrosRepository->obtenerParametro(Estado::CODIGO_ETNIA)->pluck('vc_parametro_detalle','i_pk_id')->toArray(),
            'eps'=>$this->parametrosRepository->obtenerParametro(Estado::CODIGO_EPS)->pluck('vc_parametro_detalle','i_pk_id')->toArray(),
        ]; 
        //dd($mensajes)     ;
        return view('material.sections.usuario.gestionar-usuarios',$data);         
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CrearUsuarioRequest $request) 
    { 
        
        $idUsuario=$this->usuarioRepository->crear($request);
        if($idUsuario!=-1){
            $mensajes= [
                'message'=>'El usuario ha sido creado con éxito con el código '.$idUsuario,
                'title'=>'Usuario Creado!',
                'type'=>'success'
            ];
        }else{
            $mensajes= [
                'message'=>'No se ha podido completar la creación del usuario',
                'title'=>'Operación fallida!',
                'type'=>'error'
            ];             
        } 
        return redirect('/gestionar-usuarios')->with($mensajes);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario = $this->usuarioRepository->obtenerUsuarioPorId($id); 
        $data = [ 
            
            'areas'=>$this->parametrosRepository->obtenerParametro(Estado::CODIGO_AREAS)->pluck('vc_parametro_detalle','i_pk_id')->toArray(),
            'areasPys'=>$this->parametrosRepository->obtenerParametro(Estado::CODIGO_AREAS_PAZ_Y_SALVOS)->pluck('vc_parametro_detalle','i_pk_id')->toArray(),
            'tipoDocumentos'=>$this->parametrosRepository->obtenerParametro(Estado::CODIGO_TIPO_DOCUMENTO)->pluck('vc_parametro_detalle','i_pk_id')->toArray(),
            'ciudades'=>$this->parametrosRepository->obtenerParametro(Estado::CODIGO_CIUDAD)->pluck('vc_parametro_detalle','i_pk_id')->toArray(),
            'generos'=>$this->parametrosRepository->obtenerParametro(Estado::CODIGO_GENERO)->pluck('vc_parametro_detalle','i_pk_id')->toArray(),
            'etnias'=>$this->parametrosRepository->obtenerParametro(Estado::CODIGO_ETNIA)->pluck('vc_parametro_detalle','i_pk_id')->toArray(),
            'eps'=>$this->parametrosRepository->obtenerParametro(Estado::CODIGO_EPS)->pluck('vc_parametro_detalle','i_pk_id')->toArray(),            
            'usuario'=> $usuario
        ]; 
        //dd($mensajes)     ;
        return view('material.sections.usuario.gestionar-usuarios',$data);            
        //$returnHTML="";        
        //$returnHTML = view('material.sections.usuario.form.edit',$data)->render(); 
        //return response()->json(array('success' => true, 'html'=>$returnHTML,'usuario'=>$usuario)); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ActualizarUsuarioRequest $request, $id) 
    {
        $resultado=$this->usuarioRepository->actualizar($request,$id);
        if($resultado){
            $mensajes= [
                'message'=>'Los datos del usuario ha sido modificado con éxito',
                'title'=>'Usuario Modificado!',
                'type'=>'success'
            ];
        }else{
            $mensajes= [
                'message'=>'No se ha podido completar la edición del usuario',
                'title'=>'Operación fallida!',
                'type'=>'error'
            ];            
        } 
        return redirect('/gestionar-usuarios')->with($mensajes); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function consultarUsuariosArea(Request $request){        
        $area=$request['i_fk_id_area']; 
        $u= $this->usuarioRepository->obtenerUsuariosPorArea($area);
        //dd($u);
        //return response()->json($c);  
        //$returnHTML ="";  
        $returnHTML = view('material.sections.usuario.tabla-usuarios')->with('usuarios', $u)->render();
        return response()->json(array('success' => true, 'html'=>$returnHTML,'usuarios'=>$u)); 
    }     

    public function resetPassword(Request $request){ 
        $mensaje=[]; 
        if($this->usuarioRepository->resetPassword($request['id'])){
            $mensaje=[
                'title'=>'Bien hecho',
                'message'=>'Se ha Reseteado la contraseña, ahora el usuario podrá ingresar con el documento',
                'type'=>'success'
            ];
        }
        else{
            $mensaje=[
                'title'=>'Algo ha salido mal',
                'message'=>'No ha sido posible resetear la contraseña',
                'type'=>'error'
            ];            
        }
        return response()->json($mensaje); 
    }     

    public function removerRolesUsuario(Request $request){
        $sinRoles = $this->usuarioRepository->removerRolesUsuario($request['id']);
        if($sinRoles){
            $mensaje=[
                'title'=>'Bien hecho',
                'message'=>'El usuario ha sido desactivado satisfactoriamente.',
                'type'=>'success'
            ];
        }
        else{
            $mensaje=[
                'title'=>'Algo ha salido mal',
                'message'=>'No ha sido posible desactivar el usuario',
                'type'=>'error'
            ];            
        }
        return response()->json($mensaje); 
    }

    public function activarUsuario(Request $request){
        $sinRoles = $this->usuarioRepository->activarUsuario($request['id']);
        if($sinRoles){
            $mensaje=[
                'title'=>'Bien hecho',
                'message'=>'El usuario ha sido activado satisfactoriamente.',
                'type'=>'success'
            ];
        }
        else{
            $mensaje=[
                'title'=>'Algo ha salido mal',
                'message'=>'No ha sido posible activar el usuario',
                'type'=>'error'
            ];            
        }
        return response()->json($mensaje); 
    }

    public function storeUserNoRegistrado(Request $request){

        
        $usuario = new User;
        $usuario->i_fk_tipo_documento = intval($request['i_fk_tipo_documento']);
        $usuario->i_cedula =$request['i_cedula'];
        $usuario->i_fk_area = intval($request['i_fk_area']);
        $usuario->name = $request['name'];
        $usuario->vc_segundo_nombre =$request['vc_segundo_nombre'];
        $usuario->vc_primer_apellido =$request['vc_primer_apellido'];
        $usuario->vc_segundo_apellido =$request['vc_segundo_apellido'];
        $usuario->email = $request['email'];
        $usuario->vc_telefono =$request['vc_telefono']; 
        $usuario->vc_celular = $request['vc_celular'];
        $usuario->vc_direccion =$request['vc_direccion'];
        $usuario->vc_estado = 1;
        if($usuario->save()){
            $mensajes = [
                'message'=>'Éxito en la operación',
                'title'=>'El Nuevo Usuario se creo correctamente',
                'type'=>'success',
                'flag'=> 'exito'
            ];
        }else{
            $mensajes = [
                'message'=>'No se puede realizar la operacion',
                'title'=>'No se logro crear el nuevo usuario , intentelo de nuevo',
                'type'=>'error',
                'flag'=> 'error'
            ];
 
        }

        return redirect('/creacion_usuario_no_registrado/'.$request['i_cedula'])->with($mensajes);  
    }    
}
