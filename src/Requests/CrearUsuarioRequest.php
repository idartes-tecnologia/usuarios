<?php

namespace idartes\usuario\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;

class CrearUsuarioRequest extends FormRequest
{
    //protected $errorBag = 'crear';
    public function __construct()
    {
        //$this->errorBag = 'crear';
    }     
      
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    } 

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [ 
            'i_cedula'=>'required|unique:baseadmin.tbl_users',
            'email'=>'required|email|unique:baseadmin.tbl_users',
            'i_fk_tipo_documento'=>'required',
            'name'=>'required',
            'vc_primer_apellido'=>'required',
            //'dt_fecha_nacimiento'=>'required', 
            //'vc_rh'=>'required',
            'i_fk_genero'=>'required',
            //'i_fk_eps'=>'required',
            'i_fk_ciudad'=>'required',            
        ];
    }

    public function messages()
    {
        return [
            'i_cedula.unique' => 'El documento de identidad ('.$this->i_cedula.') ya se encuentra registrado.',
            'email.unique' => 'El correo ('.$this->email.') ya se encuentra registrado.',
        ];
    } 


    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */

    /*protected function failedValidation(Validator $validator)
    {
        $this->session()->flash('form', 'crear');
        throw (new ValidationException($validator))
                    //->errorBag('crear')
                    ->errorBag($this->errorBag)
                    ->redirectTo($this->getRedirectUrl());
    } */         
}
