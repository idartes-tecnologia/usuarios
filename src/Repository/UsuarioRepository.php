<?php

namespace idartes\usuario\Repository;
use idartes\usuario\User;
use idartes\usuario\TipoPersona;
use Auth; 
use Illuminate\Support\Facades\DB;
use idartes\usuario\Repository\AuditoriaRepository as Aud;
use idartes\usuario\Repository\UsuarioInterface;
class UsuarioRepository implements UsuarioInterface{

	public function obtenerUsuarioPorId($id){
		return User::find($id);		
	}

	public function obtenerUsuarioPorCedula($documento){
		return User::where('i_cedula',$documento)->first();		
	}

	public function crear($request){
		//Para auditoría
		Aud::setUserId('baseadmin'); 
		$usuario = new User();
		$data = $request->only($usuario->getFillable());
		$data['vc_estado'] = 1; 
		$data['i_fk_eps'] = 12;
		$data['password'] = bcrypt($data['i_cedula']);
		if($usuario->fill($data)->save()){
			return $usuario->id;
		}else{
			return -1;
		}
	}

	public function actualizar($request,$id){
		//Para auditoría
		//dd($request->all());
		Aud::setUserId('baseadmin'); 		
		$usuario = User::find($id);
		$data = $request->only($usuario->getFillable());
		$data['vc_estado'] = 1; 
		$data['i_fk_eps'] = 12;
		return $usuario->fill($data)->save();
	}

	public function obtenerUsuariosPorArea($areas){ 
		return User::whereIn('i_fk_area',$areas)
			->orWhereNull('i_fk_area')
			->with('area')
			->get();  	 
	}   

	public function obtenerUsuariosActivos(){
		return User::where('vc_estado',1)->get()->pluck('fullname','id')->toArray();
	}
	
	public function obtener($id, $relaciones = []){}
	public function eliminar($id){}
	public function obtenerTodo($relaciones = []){}
	public function dataTable($relaciones = []){}	

    public function resetPassword($id){        
        $usuario = User::find($id);
		$usuario->password = bcrypt($usuario->i_cedula);
		return $usuario->save();
	} 	
	
	public function removerRolesUsuario($id)
	{	
		$sinRoles = 0;
		$usuario = User::find($id);
		if(is_object($usuario)){
			$usuario->vc_estado = 0;
			if($usuario->save()){				
				//TipoPersona::where('user_id', $id)->delete();
				$sinRoles = 1;
			}
		}
		return $sinRoles;
	}
	
	public function obtenerPorRol($i_rol)
	{
		return TipoPersona::where('tipo_id',$i_rol)
			->get();
	}

	public function obtenerEmailUsuariosActivos(){
		return User::where('vc_estado',1)
					->where('email', '!=', '')
					->get()
					->pluck('fullname','email')
					->toArray();
	}

	public function activarUsuario($id)
	{	
		$sinRoles = 0;
		$usuario = User::find($id);
		if(is_object($usuario)){
			$usuario->vc_estado = 1;
			
			if($usuario->save()){
            	//TipoPersona::onlyTrashed()->where('user_id', $id)->restore();
				//$tipoPersonas = TipoPersona::withTrashed()->where('user_id', $id)->get();
				//foreach ($tipoPersonas as $tipoPersona) {
				//	$tipoPersona->restore();
				//}
				$sinRoles = 1;
			}
		}
		return $sinRoles;
	}
	
	public function obtenerUsuariosPorRoles($roles){
		$usuarios = User::where('vc_estado',1)
					->where('email', '!=', '')
					->whereHas('tiposPersona',function($query) use($roles){
						$query->whereIn('tbl_tipo_persona.tipo_id',$roles);
					})					
					->get();
		return $usuarios;
	}
}