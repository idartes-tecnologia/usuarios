<?php

namespace idartes\usuario\Repository;
use idartes\usuario\User;
use idartes\usuario\ParametroDetalles;
use Illuminate\Support\Facades\DB;

class ParametroDetalleRepository
{

    static function tipoUsuario($tipo)
    {
        $estado = '';
        switch ($tipo) {
            case 1:
                $estado = 'SUPERVISOR';
            break;
            case 2:
                $estado = 'JEFE AREA';
            break;
            case 3:
                $estado = 'APOYO';
            break;
            default:
                $estado = 0;
            break;
        }

        return $estado;
    }


    static function tipoEstado($tipo)
    {
        $estado = '';
        switch ($tipo) {
            case 0:
                $estado = 'NO';
            break;
            case 1:
                $estado = 'SI';
            break;

            default:
                $estado = 0;
            break;
        }

        return $estado;
    }

    static function obtenerAnios(){
        $anios=array();
        for ($i=date("Y")-10; $i <= date("Y") ; $i++) { 
            $anios[$i]=$i;
        }
        return $anios;        
    }

    static function obtenerUsuarios(){
        return User::where('vc_estado','=',1)->get()->pluck('full_name', 'id')->toArray();
    }

    static function obtenerTablasConTriggers(){
        $tablas = DB::select('SELECT  DISTINCT T.event_object_table FROM INFORMATION_SCHEMA.TRIGGERS T');
        $arrayTablas = [];

        foreach ($tablas as $key => $value) {
            $arrayTablas[$value->event_object_table]=$value->event_object_table;
        }
        return $arrayTablas;
    }

    static function obtener($id){
        return ParametroDetalles::find($id);
    } 
    static function obtenerParametro($parametro){
        return ParametroDetalles::with('hijos')
        		->where('i_fk_id_parametro',$parametro)
        		->where('i_estado',1)
                ->orderBy('vc_parametro_detalle','ASC')->get();         
    }

    static function obtenerParametros($parametro){
        return ParametroDetalles::with('hijos')
        		->whereIn('i_pk_id',$parametro)
        		->where('i_estado',1)
                ->orderBy('vc_parametro_detalle','ASC')->get();         
    }

    static function obtenerParametroHijosPorParametro($arrPadres, $parametro){ 
        return ParametroDetalles::whereIn('i_fk_id_padre',$arrPadres)
                ->where('i_fk_id_parametro',$parametro)
        		->where('i_estado',1)
        		->orderBy('vc_parametro_detalle','ASC')->get();
    }

    static function obtenerParametroHijos($arrPadres){ 
        return ParametroDetalles::whereIn('i_fk_id_padre',$arrPadres)
        		->where('i_estado',1)
        		->orderBy('vc_parametro_detalle','ASC')->get();
    } 
    
    static function getParametroDetalle($id){
        return ParametroDetalles::where('i_pk_id',$id)->get();
    }

    static function obtenerParametroPadre($parametro){
        return ParametroDetalles::with('hijos')
        		->where('i_fk_id_parametro',$parametro)
                ->where('i_estado',1)
                ->where('i_fk_id_padre',null)
                ->orderBy('vc_parametro_detalle','ASC')->get();         
    }

    static function obtenerParametrosHijo($parametro){
        return ParametroDetalles::with('hijos')
        		->where('i_fk_id_parametro',$parametro)
                ->where('i_estado',1)
                ->where('i_fk_id_padre','<>',null)
                ->orderBy('vc_parametro_detalle','ASC')->get();         
    }   
    
    static function obtenerDetallesPorParametroWhere($parametro){
        return Parametro::whereIn('i_pk_id',$parametro)
                ->with(['detalles'=>function($query) {
                    $query-> where('i_estado',1);
                }])->get();

    }
    
    static function obtenerPorCodigoInterno($id){
        return ParametroDetalles::where('i_codigo_interno',$id)->first();
    } 
    static function obtenerParametroPorCodigoInterno($parametro){
        return ParametroDetalles::with('hijos')
                ->whereHas('parametro',function($query) use ($parametro){
                    $query->where('i_codigo_interno',$parametro);
                })
        		->where('i_estado',1)
                ->orderBy('vc_parametro_detalle','ASC')->get();         
    }

    static function obtenerParametrosPorCodigoInterno($parametros){
        return ParametroDetalles::with('hijos')
                ->whereHas('parametro',function($query) use ($parametros){
                    $query->whereIn('i_codigo_interno',$parametros);
                })
        		->where('i_estado',1)
                ->orderBy('vc_parametro_detalle','ASC')->get();         
    }    
    
} 