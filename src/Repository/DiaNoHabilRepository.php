<?php

namespace App\Repositories;

use App\DiaNoHabil;
use Carbon\Carbon;

class DiaNoHabilRepository
{
    /**
     * Obtiene el listado de días no hábiles por vigencia (año).
     *
     * @param int $vigencia
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function obtenerDiasNoHabilesPorVigencia($vigencia)
    {
        return DiaNoHabil::whereYear('d_dia', $vigencia)->get();
    }

    /**
     * Calcula la fecha anterior a la recibida descontando un número de días hábiles.
     *
     * @param string $fecha
     * @param int $diasHabiles
     * @return string
     */
    public function calcularFechaDiasAnteriorNoHabiles($fecha, $diasHabiles)
    {
        $fecha = Carbon::parse($fecha);
        //Obtener el año de la fecha
        $vigencia = $fecha->year;
        //Obtener los días no hábiles de la vigencia
        $diasNoHabiles = $this->obtenerDiasNoHabilesPorVigencia($vigencia)->pluck('d_dia')->toArray();

        while ($diasHabiles > 0) {
            $fecha->subDay();

            // Si es fin de semana o día no hábil, continuar el bucle
            if ($fecha->isWeekend() || in_array($fecha->toDateString(), $diasNoHabiles)) {
                continue;
            }

            // Reducir el contador de días hábiles
            $diasHabiles--;
        }

        return $fecha->toDateString();
    }
}
