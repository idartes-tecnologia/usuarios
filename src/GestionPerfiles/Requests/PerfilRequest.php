<?php

namespace idartes\usuario\GestionPerfiles\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PerfilRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vc_tipo'=>'required'
        ];
    }
    public function messages()
    {
        return [
            'vc_tipo.required' => 'El nombre del perfil es requerido'
        ];
    }    
}
