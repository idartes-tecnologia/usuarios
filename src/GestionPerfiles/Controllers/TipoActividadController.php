<?php

namespace idartes\usuario\GestionPerfiles\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use idartes\usuario\GestionPerfiles\Requests\TipoActividadRequest;
use idartes\usuario\Tipo;
use idartes\usuario\Modulo;
use idartes\usuario\Actividad;

use DB;

class TipoActividadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


 

    public function index()
    {
        //
        $tipoPersona = Tipo::all()->pluck('vc_tipo', 'i_pk_id')->toArray();
        $modulo = Modulo::all()->pluck('vc_modulo', 'i_pk_id')->toArray();
        $actividades = null;
        $tipo_actividades = null;
        $i_tipo = null;

        return view('material.sections.perfiles.tipo-actividad',compact('tipoPersona','modulo','actividades','tipo_actividades','i_tipo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        
        return null;
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(TipoActividadRequest $request)
    {
        //
        //$request['tipoPersona'];
        
        $actividades = Modulo::find($request['modulo']);
        $tipoPersona = Tipo::all()->pluck('vc_tipo', 'i_pk_id')->toArray();
        $modulo = Modulo::all()->pluck('vc_modulo', 'i_pk_id')->toArray();
        $tipo_actividades = Tipo::find($request['tipoPersona']);
        $i_tipo = $request['tipoPersona'];
        return view('material.sections.perfiles.tipo-actividad',compact('tipoPersona','modulo','actividades','tipo_actividades','i_tipo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
      
    }

    public function add(Request $request,$id_act,$id_tipo)
    {
        $tipo = Tipo::find($id_tipo);
        $tipo->actividades()->attach($id_act,[
                        'i_estado'=>1,
                    ]);
      
        return "Bien !!";
    }

    public function remove(Request $request,$id_act,$id_tipo)
    {
        $tipo = Tipo::find($id_tipo);

        $tipo->actividades()->detach($id_act);
      
        return "Eliminada !!";
    }
}
