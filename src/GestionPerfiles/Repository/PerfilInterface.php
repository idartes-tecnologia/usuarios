<?php

namespace idartes\usuario\GestionPerfiles\Repository;

use idartes\usuario\Repository\CRUDInterface;

interface PerfilInterface extends CRUDInterface
{
	public function obtenerCargosActivos($clasePerfil);
}