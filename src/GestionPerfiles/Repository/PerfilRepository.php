<?php

namespace idartes\usuario\GestionPerfiles\Repository;
use idartes\usuario\Tipo;

use idartes\usuario\GestionPerfiles\Repository\PerfilInterface;
use Illuminate\Support\Facades\Schema;
use idartes\usuario\TipoPersona;

class PerfilRepository implements PerfilInterface{


	public function obtener($id,$relaciones = []){
		return Tipo::find($id);
	}

	public function obtenerNombreEstadoTblTipo($perfil,$estadoPerfil){
		return Tipo::where('vc_tipo', '=',$nombrePerfil)
        			->where('i_estado', '=',$estadoPerfil)->first();
    }

    public function mostrarTabla(){
    	return Tipo::with('modulos')->orderBy('i_pk_id', 'DESC')->get();
    }
    public function crear($request)
    {
		try{
	    	$perfil = new Tipo();
	    	$data = $request->only($perfil->getFillable());
	    	$data['i_estado'] = (isset($request->i_estado)) ? 1 : 0;
				$data['i_requisitos'] = (isset($request->i_requisitos)) ? 1 : 0;
	    	return $perfil->fill($data)->save(); 
		}catch(\Exception $e){
			echo $e->getMessage(); 
			dd($e);
			return 0;
		}    			 	
    }
   	public function actualizar($request,$id){
    	try{
	    	$perfil = Tipo::find($id);
	    	$data = $request->only($perfil->getFillable());
	    	$data['i_estado'] = (isset($request->i_estado)) ? 1 : 0;
				$data['i_requisitos'] = (isset($request->i_requisitos)) ? 1 : 0;
	    	return $perfil->fill($data)->save();
	    	/*$perfil->vc_tipo= $request->vc_tipo;
	    	$perfil->i_estado= $request->i_estado;
	    	return $perfil->save();*/
		}catch(\Exception $e){
			echo $e->getMessage(); 
			return 0;
		}		    	
    }
    public function eliminar($id){
    	return Tipo::find($id)->delete();
    }

	public function obtenerTodo($relaciones = []){}
	public function dataTable($relaciones = []){}  

    public function obtenerPerfilesPorModulo($modulo,$clasePerfil){
        $cargos = Tipo::where('i_fk_id_modulo',$modulo)
        		->where('i_estado',1)
        		->where('i_fk_id_clase',$clasePerfil)
        		->whereHas('tipoPersonas',function($query){
        			$query->where('tbl_tipo_persona.i_estado',1);
        		})
				->with(['tipoPersonas'=>function($query){
					$query->with('usuario');
				}])
        		->get();
        //return $cargos;

		$options = [];        		
        foreach ($cargos as $cargo) {
        	$options[$cargo->i_pk_id] = $cargo->vc_tipo." - ".$cargo->tipoPersonas[0]->usuario->fullname;
        }
        return $options;
    }
	
    public function obtenerCargosActivos($clasePerfil){
        
		$tipo = new TipoPersona();
		$tabla = $tipo->getTable();
		$conexion = $tipo->getConnectionName();
		$options = [];     
		if (Schema::connection($conexion)->hasColumn($tabla, 'dt_fecha_posesion') &&
			Schema::connection($conexion)->hasColumn($tabla, 'dt_fecha_retiro')) {				
			$hoy = date('Y-m-d H:i:s');
			$cargos = Tipo::where('i_fk_id_clase',$clasePerfil)
					->whereHas('tipoPersonas',function($query) use($hoy){
						$query
							->where('tbl_tipo_persona.dt_fecha_posesion','<=',$hoy)
							->where('tbl_tipo_persona.dt_fecha_retiro','>=',$hoy);
					})
					->with(['tipoPersonas'=>function($query){
						$query->with('usuario');
					}])
					->get();

			foreach ($cargos as $cargo) {
				foreach($cargo->tipoPersonas as $tipoPersona){
					if($tipoPersona->dt_fecha_posesion <= $hoy && $tipoPersona->dt_fecha_retiro >= $hoy){
						if($tipoPersona->usuario->vc_estado == 1){
							$options[$tipoPersona->usuario->id] = $cargo->vc_tipo." - ".$tipoPersona->usuario->fullname;
						}
					}
				}
			}					
		}else{
			$cargos = Tipo::where('i_fk_id_clase',$clasePerfil)
					->whereHas('tipoPersonas',function($query){
						$query->where('tbl_tipo_persona.i_estado',1);
					})
					->with(['tipoPersonas'=>function($query){
						$query->with('usuario');
					}])
					->get();

			foreach ($cargos as $cargo) {
				foreach($cargo->tipoPersonas as $tipoPersona){
					if($tipoPersona->i_estado == 1){
						if($tipoPersona->usuario->vc_estado == 1){
							$options[$tipoPersona->usuario->id] = $cargo->vc_tipo." - ".$tipoPersona->usuario->fullname;
						}
					}
				}
			}					
		}
        return $options;
    }	
}